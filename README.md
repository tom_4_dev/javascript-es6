### What is this repository for? ###

* Complete guide for ES6

### Strings and Template Literals ###

* [Learn String with ES6](https://codepen.io/santanu-bhattacharya/pen/PeeNNg?editors=0012)

### Block Bindings ###

* [Learn Block Bindings with ES6](https://codepen.io/santanu-bhattacharya/pen/RyyGxx?editors=0012)
* [Block and Binding](https://codepen.io/santanu-bhattacharya/pen/MGXxMX?editors=0011)

### Functions ###
* [default parameter](https://codepen.io/santanu-bhattacharya/pen/vjrMdg?editors=0012)
* [rest parameter](https://codepen.io/santanu-bhattacharya/pen/aGKxaO?editors=0012)

